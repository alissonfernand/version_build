import 'dart:async';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info0.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PackageInfo Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyHomePage(title: 'PackageInfo example app'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

 final _keyScaffold = GlobalKey<ScaffoldState>();

 final String versionApp = '1';
 final String versionApp2 = '1';

class _MyHomePageState extends State<MyHomePage> {

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

   

  @override
  void initState()   {
    super.initState();
    

    _initPackageInfo();

    WidgetsBinding.instance.addPostFrameCallback((_){
      Future.delayed(Duration(seconds: 2)).then((_){
        if( _packageInfo.buildNumber == versionApp )
        showDialog(
      context: context,
      builder: (context) =>
      AlertDialog(
          title: Text('Nova Atualização'),
          content: Text('Existi nova Atualização'),
          actions: <Widget>[
            FlatButton(onPressed: (){}, child: Text('Cancelar'))
          ],
        )
      );
      });
      
    });
   
    
  
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

   _showDialog() {
    showDialog(
      context: context,
      builder: (context){
        return AlertDialog(
          title: Text('Nova Atualização'),
          content: Text('Existi nova Atualização'),
          actions: <Widget>[
            FlatButton(onPressed: (){}, child: Text('Cancelar'))
          ],
        );
      }
      );
  }

 

  Widget _infoTile(String title, String subtitle) {
    return ListTile(
      title: Text(title),
      subtitle: Text(subtitle ?? 'Not set'),
    );
  }

  @override
  Widget build(BuildContext context) {

   return Scaffold(
     body:Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
         InkWell(
          child: Text('Click'),
          onTap: () =>  launch('https://play.google.com/store/apps/details?id=br.com.xopedir'),
        ),
          _infoTile('App name', _packageInfo.appName),
          _infoTile('Package name', _packageInfo.packageName),
          _infoTile('App version', _packageInfo.version),
          _infoTile('Build number', _packageInfo.buildNumber),
        ],
      ),
    )
   );
    

  }
}

